import { Injectable, LOCALE_ID, Inject  } from '@angular/core';
import {Http, Headers,RequestOptions} from '@angular/http';
import { LoopBackAuth, LoopBackConfig }  from './../../sdk/index';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';


@Injectable()
export class MultipartService {

	base:string;
	urls: any;

	constructor(@Inject(LOCALE_ID) protected localeId: string, private http:Http,private auth:LoopBackAuth) { 
		this.base = LoopBackConfig.getPath();
		this.urls = {
			dealership 	: this.base + "/api/RequestDealerships/request",
			employemt 	: this.base + "/api/RequestEmployments/request",
			vehicleType : this.base + "/api/VehicleTypes/getTypesByEveryone?ln="+localeId,
			vehicle 	: this.base + "/api/RequestVehicles/request",
			contact 	: this.base + "/api/People/contactUs",
			signup 		: this.base + "/api/WebUsers/signup",
			getMyInfo 	: this.base + "/api/WebUsers/getMyInfo"
		};
	}

	request(url,data){
		console.log("data : ", data);
		return this.http.post(url,this.getFormObj(data),this.getOption()).map((res: any) => (res.text() != "" ? res.json() : {})).catch((error) => {return Observable.throw(error.json().error || 'Server error');});
	}

	getFormObj(obj){
		let fd = new FormData();
		if(obj.data && typeof obj.data == 'object'){
			fd.append('data',JSON.stringify(obj.data));
		}
		for(let x in obj.files){
			if(obj.files[x].constructor === Array){
				for(let y in obj.files[x]){
					fd.append(x,obj.files[x][y][0]);
				}
			}else{
				if(typeof obj.files[x] === 'string'){
					fd.append(x,obj.files[x]);
				}else{
					for(let y in obj.files[x]){
						fd.append(x,obj.files[x][y]);   
					}
				}
			}
		}
		return fd;
	}

	getOption(){
		let header : Headers = new Headers();
		header.append('Authorization',this.auth.getAccessTokenId() || "");
		let opts: RequestOptions = new RequestOptions();
		opts.headers = header;
		return opts;
	}

	dealershipApi(data){
		return this.request(this.urls.dealership,data);
	}
	
	employmentApi(data){
		return this.request(this.urls.employemt,data);
	}
	
	vehicleApi(data){
		return this.request(this.urls.vehicle,data);
	}
	
	getVehicleType(): Observable<any>{
		let apiUrl = this.urls.vehicleType;
		return this.http.get(apiUrl)
			.map(res=>{
				return res.json();
			});
	}
	
	contactUs(name,email,contactNumber,message) : Observable<any>{
		let apiUrl = this.urls.contact;
		let data = { 
			name : name,
			email : email,
			contactNumber : contactNumber,
			message : message
		};
		return this.http.post(apiUrl,data)
			.map(res=>{
				return res.json();
			});
	}
	
	signupApi(data){
		return this.request(this.urls.signup,data);
	}
	
	getMyInfo(): Observable<any>{
		let apiUrl = this.urls.getMyInfo;
		return this.http.post(apiUrl,{},this.getOption())
			.map(res=>{
				return res.json();
			});
	}
}
