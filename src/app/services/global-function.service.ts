import { Injectable } from '@angular/core';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router } from '@angular/router';
import { LoopBackAuth } from './../sdk/index';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class GlobalFunctionService {

	authCred : any = {};
	customerData : any = {};
	token : any = '';
	constructor(private toastr : ToastsManager, private router : Router, private auth : LoopBackAuth) { }
	
	// Observable string sources
	private componentMethodCallSource = new Subject<any>();

	// Observable string streams
	componentMethodCalled$ = this.componentMethodCallSource.asObservable();

	// Service message commands
	callComponentMethod(){
		this.componentMethodCallSource.next();
	}
  
	successToast(mess1,mess2){
		this.toastr.success(mess1,mess2,{showCloseButton : true, toastLife : 3000});
	}
	
	errorToast(mess1,mess2){
		this.toastr.error(mess1,mess2,{showCloseButton : true, toastLife : 5000});
	}
	
	warningToast(mess1,mess2) {
        this.toastr.warning(mess1, mess2);
    }
    
    infoToast(mess1) {
        this.toastr.info(mess1,'',{toastLife : 1200});
    }
	
	logoutToast(mess1){
		this.toastr.info(mess1,'',{toastLife : 1200});
	}
     
    customToast(mess1,mess2){
        this.toastr.custom('<span style="color: red">'+ mess1 +'</span>', null, {enableHTML: true});
    }
	
	navigateToError(error){
		if(error === 'server'){
			this.router.navigate(['server-error']);
		}
		if(error === '401'){
			this.auth.clear();
			this.router.navigate(['/']);
		}
	}
	
	
	
	getUserCredentials(){
		this.authCred.tokenId = this.auth.getAccessTokenId() || '';
		this.authCred.userId = this.auth.getCurrentUserId() || '';
		this.authCred.realm = this.auth.getCurrentUserData() ? this.auth.getCurrentUserData().realm : '';
		return this.authCred;
	}
	
	setToken(token){
		this.token = token;
		console.log("token : ", this.token);
	}
	
	getToken(){
		return this.token;
	}

	setLoopbackAuth(data){
		let token : any = {};
		if(data.id){
			token.id = data.id;
			token.rememberMe = false;
			token.ttl = data.ttl;
			token.created = data.user.created;
		}
		token.userId = data.user.id;
		token.user = data.user;
		this.auth.setToken(token);
	}
}
