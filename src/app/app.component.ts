import { Component,HostListener,ElementRef, Output,ViewChild, EventEmitter,ViewContainerRef, OnInit , LOCALE_ID, Inject} from '@angular/core';
import { LoopBackConfig,LoopBackAuth,PeopleApi } from './sdk/index';
import { ToastsManager } from 'ng2-toastr/ng2-toastr';
import { Router, NavigationEnd } from '@angular/router';
import { GlobalFunctionService } from './services/global-function.service';

import { LoginComponent } from './components/login/login.component';
import { SignUpComponent } from './components/sign-up/sign-up.component'
import { SignUpAgentComponent } from './components/sign-up-agent/sign-up-agent.component';
import { OtpComponent } from './components/otp/otp.component';
import { RahResetPasswordRequestComponent } from './components/rah-reset-password-request/rah-reset-password-request.component';
import { RahResetPasswordComponent } from './components/rah-reset-password/rah-reset-password.component';
import { ResetOtpComponent } from './components/reset-otp/reset-otp.component';



declare var $:any;
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
	
	authCred : any = {};
	isLogin : boolean = false;
	baseUrl : string;
	defaultImage : string;
	myInfo : any;
	
	hideTopBar : string[];
	hideHeader : boolean = false;
	passObj:any = {};
	cpResError:any = {};

		
	@ViewChild(LoginComponent)
	private loginModal : LoginComponent;
	
	@ViewChild(SignUpComponent)
	private signupModal : SignUpComponent;
	
	@ViewChild(SignUpAgentComponent)
	private signupAgentModal : SignUpAgentComponent;
	
	@ViewChild(OtpComponent)
	private otp : OtpComponent;

	@ViewChild(RahResetPasswordRequestComponent)
	private resetRequest : RahResetPasswordRequestComponent;

	@ViewChild(ResetOtpComponent)
	private resetOtpComp : ResetOtpComponent;

	@ViewChild(RahResetPasswordComponent)
	private resetPassComp : RahResetPasswordComponent;

	visitorsCount : any = 0;
	errMessages :any = {
		"Invalid current password":"अमान्य वर्तमान पासवर्ड"
	}
	/*@HostListener('window:onload', ['$event'])
		beforeunloadHandler(e){
			console.log("called");
			let myStorage = window.sessionStorage;
			if(!myStorage.getItem('visitorsCount')){
				//this.visitorsCount = +myStorage.getItem('visitorsCount') + 1;
				this.updateCounter();
			}else{
				//myStorage.setItem('visitorsCount',this.visitorsCount);
				//this.setVisitors();
			}
		}*/
	
	constructor(@Inject(LOCALE_ID) protected localeId: string, private globalFunctionService : GlobalFunctionService,private peopleApi : PeopleApi, private router : Router, private eRef : ElementRef,public toastr : ToastsManager, private vcr: ViewContainerRef, private auth : LoopBackAuth){
		LoopBackConfig.setBaseURL('https://api.babbabazri.com'); //http://139.59.71.150:3010
		// LoopBackConfig.setBaseURL('http://139.59.71.150:3010');
		LoopBackConfig.setApiVersion('api'); 
		this.toastr.setRootViewContainerRef(vcr);
		this.baseUrl = LoopBackConfig.getPath();
		// this.defaultImage = 'assets/images/default-pic.png';
		
		// this.hideTopBar = ['reset','reset-password'];
		
		this.router.errorHandler = (error: any) => {
			this.router.navigate(['/']);
		}
		
		let currentUrl = this.router.url;
		this.setAuthCredential();
		let _self = this;
		this.router.events.subscribe((evt) => {
			this.setAuthCredential();
			//this.setVisitors();
			if (!(evt instanceof NavigationEnd)){
				return;
			}
			// if(this.authCred.tokenId && ((this.authCred.realm === 'engineer' || this.authCred.realm === 'vendor') && evt.url == '/')){
				// _self.router.navigate(['/dashboard']);
			// }
			if(evt.url === '/' ){
				//_self.updateCounter();
			}
			if(!this.authCred.tokenId && evt.url.includes('dashboard')){
				_self.router.navigate(['/']);
			}
		});
		
	}
	
	setAuthCredential(){
		// let myStorage = window.localStorage;
		// this.visitorsCount = +myStorage.getItem('visitorsCount');
		this.authCred.tokenId = this.auth.getAccessTokenId();
		this.authCred.userId = this.auth.getCurrentUserId();
		this.authCred.realm = this.auth.getCurrentUserData() ? this.auth.getCurrentUserData().realm : '';
		if(this.authCred.tokenId && this.authCred.userId){
			this.isLogin = true;
		}
		else{
			this.isLogin = false;
		}
	}
		

	getCurrentRoute() {
		console.log(this.router.url)
	    return this.router.url;
	}

	// ********************************* change password ******************************
	// this function use when we are changing password using old password 	
	
	openChangePassword(kaiCPForm){
		this.cpResError.isError = false;
	    this.cpResError.msg = "";
	    this.cpResError.isSuccess = false;
	    this.cpResError.successMsg = "";
		kaiCPForm.resetForm();
		$("#kaiCPModal").modal("show");

	}

	changePassword(cpForm){
      this.cpResError.isError = false;
      this.cpResError.msg = "";
      this.cpResError.isSuccess = false;
      this.cpResError.successMsg = "";
      if(cpForm.valid){
          let btn = $("#cpBtn");  
          btn.button('loading');
          this.peopleApi.changePassword(this.passObj.oldPassword,this.passObj.newPassword).subscribe((success)=>{
            btn.button('reset');
            $('#cpModal').modal("show");
            this.cpResError.isSuccess = true;
            this.cpResError.successMsg = this.localeId == "hi"? "पासवर्ड सफलतापूर्वक बदल गया" :"Password change successfully";
            cpForm.resetForm();
          },(error)=>{
          	this.cpResError.isError = true;
          	if(this.localeId == "hi"){
				if(this.errMessages[error.message]){
					this.cpResError.msg = this.errMessages[error.message];
				}else{
					this.cpResError.msg = error.message;	
				}	
			}
			else{
				this.cpResError.msg = error.message;	
			}
            btn.button('reset');
            
            
          })
      }    		

	}


	// ********************************* change password ******************************

	updateCounter(){
		console.log("called");
		let _self = this;
		let myStorage = window.sessionStorage;
		this.visitorsCount = +myStorage.getItem('visitorsCount');
		if(!this.visitorsCount){
			this.peopleApi.updateCount({}).subscribe(
				(success)=>{
					console.log("counter : ", success);
					_self.visitorsCount = success.success.data.count;
					let myStorage = window.sessionStorage;
					myStorage.setItem('visitorsCount',_self.visitorsCount);
				},
				(error)=>{
					console.log("error : ", error);
				}
			);
		}else{
			
		}
	}
	
	setVisitors(){
		let myStorage = window.sessionStorage;
		this.visitorsCount = +myStorage.getItem('visitorsCount');
	}
	
	logOut(){
		let _self = this;
		this.peopleApi.logout().subscribe(
			(success)=>{
				console.log("log out : ", success);
				_self.auth.clear();
				_self.globalFunctionService.logoutToast('Logging you out...');
				_self.setAuthCredential();
				_self.router.navigate(['/']);
			},
			(error)=>{
				if(error === 'Server error'){
					_self.globalFunctionService.navigateToError('server');
				}else if(error.statusCode === 401){
					_self.globalFunctionService.navigateToError('401');
				}else{
					console.log('error : ', error);
					//_self.globalFunctionService.errorToast(error.message,'oops');
				}
				console.log("error : ", error);
			}
		);
	}
	
	ngOnInit(){
		this.updateCounter();

		$("#flip1").click(function(){
	        $("#panel1").slideToggle("slow");
	    });

	    $("#flip2").click(function(){
	        $("#panel2").slideToggle("slow");
	    });

		$(window).scroll(function () {
			if ($(this).scrollTop() > 250) {
				$('.top-scroll').fadeIn();
			} else {
				$('.top-scroll').fadeOut();
			}
		});

		$('.top-scroll').click(function (){
			$("html, body").animate({
				scrollTop: 0
			}, 600);
			return false;
		}); 
		
		$(".navbar-icon").click(function() {
			$("body").addClass("display-mobile-menu");
		});
		$(".close-canvas-mobile-panel").click(function() {
			$("body").removeClass("display-mobile-menu");
		});
		
		let currentUrl = this.router.url;
		let _self = this;
		this.router.events.subscribe((evt) => {
			if (!(evt instanceof NavigationEnd)) {
				return;
			}
		});
	}
	
	toggleDropdown(){
		if($('.navbar-collapse').hasClass('show')){
			$('.navbar-collapse').removeClass('show');
		}else{
			$('.navbar-collapse').addClass('show');
		}
	}
	
	navigateToSame(){
		window.location.replace(window.location.href);
	}
	
	openLoginModal(){
		this.loginModal.openModal();
	}
	
	openUserSignUpModal(){
		this.signupModal.openModal();
	}
	
	openAgentSignUpModal(){
		this.signupAgentModal.openModal();
	}
	
	resendOtp(data){
		this.otp.resendOtp(data.peopleId,data.type);
	}
	
	openOtpModal(data){
		this.otp.openModal(data.peopleId);
	}

	openResetModal($event?:any){
		this.loginModal.closeModal();
		this.resetRequest.openModel();
	}

	openResetOtpModal(data){
		this.resetRequest.closeModel();
		this.resetOtpComp.openModal(data.peopleId);
	}

	openCPModal(data){
		this.resetOtpComp.closeModal();
		this.resetPassComp.openModal(data);
	}


}