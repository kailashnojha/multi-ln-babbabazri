import { BrowserModule } from '@angular/platform-browser';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { SDKBrowserModule } from './sdk/index';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { StartComponent } from './components/start/start.component';
// import { MatKeyboardModule } from '@ngx-material-keyboard/core';
// import { FilterRequestPipe } from './pipes/filter-request.pipe';
import { ToastModule } from 'ng2-toastr/ng2-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { GlobalFunctionService } from './services/global-function.service';
import { FileDirective } from './directives/file/file.directive';
import { MultipartService } from './services/multipart/multipart.service';

import { TestDirectiveDirective } from './directives/test-directive.directive';
import { TestDirectiveDirective1 } from './directives/test-directive1.directive';
import { TestDirectiveDirective2 } from './directives/test-directive2.directive';

import { AgmCoreModule } from '@agm/core';
// import { FilterBankListPipe } from './pipes/filter-bank-list.pipe';
import { ServerErrorComponent } from './components/server-error/server-error.component';
import { ForgetPasswordComponent } from './components/forget-password/forget-password.component';
import { NewPasswordComponent } from './components/new-password/new-password.component';

import { environment } from './../environments/environment';
import { EmploymentComponent } from './components/employment/employment.component';
import { DealershipComponent } from './components/dealership/dealership.component';
import { VehicleComponent } from './components/vehicle/vehicle.component';
import { ContactComponent } from './components/contact/contact.component';
import { LoginComponent } from './components/login/login.component';
import { SignUpComponent } from './components/sign-up/sign-up.component';
import { SignUpAgentComponent } from './components/sign-up-agent/sign-up-agent.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';
import { OtpComponent } from './components/otp/otp.component';
import { RahResetPasswordComponent } from './components/rah-reset-password/rah-reset-password.component';
import { RahResetPasswordRequestComponent } from './components/rah-reset-password-request/rah-reset-password-request.component';
import { ResetOtpComponent } from './components/reset-otp/reset-otp.component';

@NgModule({
	declarations: [
		AppComponent,
		StartComponent,
		FileDirective,
		TestDirectiveDirective,
		TestDirectiveDirective1,
		TestDirectiveDirective2,
		ServerErrorComponent,
		ForgetPasswordComponent,
		NewPasswordComponent,
		EmploymentComponent,
		DealershipComponent,
		VehicleComponent,
		ContactComponent,
		LoginComponent,
		SignUpComponent,
		SignUpAgentComponent,
		DashboardComponent,
		AboutUsComponent,
		PrivacyPolicyComponent,
		OtpComponent,
		RahResetPasswordComponent,
		RahResetPasswordRequestComponent,
		ResetOtpComponent
	],
	imports: [
		BrowserAnimationsModule,
		BrowserModule,
		AppRoutingModule,
		HttpModule,
		SDKBrowserModule.forRoot(),
		BrowserAnimationsModule,
		ToastModule.forRoot(),
		FormsModule,
		ReactiveFormsModule,
		// MatKeyboardModule,
		AgmCoreModule.forRoot({
			apiKey: "AIzaSyAKqYlReSqm0QRM8Zi7XjwRQBT6Q85uhDA",
			libraries: ["places"]
		}),
	],
	providers: [
		GlobalFunctionService,
		MultipartService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }