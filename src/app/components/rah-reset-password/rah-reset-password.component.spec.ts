import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RahResetPasswordComponent } from './rah-reset-password.component';

describe('RahResetPasswordComponent', () => {
  let component: RahResetPasswordComponent;
  let fixture: ComponentFixture<RahResetPasswordComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RahResetPasswordComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RahResetPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
