import { Component, OnInit , ViewChild, LOCALE_ID, Inject} from '@angular/core';
import { PeopleApi } from './../../sdk';
declare var $:any;

@Component({
  selector: 'app-rah-reset-password',
  templateUrl: './rah-reset-password.component.html',
  styleUrls: ['./rah-reset-password.component.css']
})
export class RahResetPasswordComponent implements OnInit {

  user:any = {};	
  actionResError:any = {};
  data:any = {};
  @ViewChild("cpForm") cpForm;
  constructor(@Inject(LOCALE_ID) protected localeId: string, private peopleApi:PeopleApi) { }

  ngOnInit() {
  	
  }

  openModal(obj){
    this.actionResError.isError = false;
    this.actionResError.isSuccess = false;
    this.cpForm.resetForm();
  	// console.log(obj)
  	this.data = obj;
  	$("#rah-reset-password-modal").modal("show");
  }

  resetPassword(cpForm){
  	this.actionResError.isError = false;
  	this.actionResError.isSuccess = false;
  	if(cpForm.valid){
  		let btn = $("#cpBtn");
      	btn.button('loading');
	  	if(this.user.password == this.user.conPassword){
		  	this.peopleApi.resetPassword(this.data.peopleId,parseInt(this.data.otp),this.user.password).subscribe((success)=>{
		  		this.actionResError.isSuccess = true;
        	this.actionResError.successMsg = this.localeId == "hi" ? "पासवर्ड सफलतापूर्वक रीसेट" : "Password reset successfully";
          cpForm.resetForm();
        		
		  	},(error)=>{
		  		this.actionResError.isError = true;
        		this.actionResError.msg = error.message;
		  	})	
		}else{
		  	this.actionResError.isError = true;
        this.actionResError.msg = this.localeId == "hi" ? "पासवर्ड और पुष्टि पासवर्ड मेल नहीं खाता है" : "Password and change password does not match";
		}
  	}
  	
  }



}
