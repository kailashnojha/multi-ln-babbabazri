import { Component, OnInit, Output, EventEmitter, LOCALE_ID, Inject } from '@angular/core';
import { ReactiveFormsModule,FormsModule,FormGroup,FormControl,FormArray,Validators,FormBuilder } from '@angular/forms';

import { validateEmail } from './../../validators/email.validator';
import { validateMobile } from './../../validators/mobile.validator';

import { MultipartService } from './../../services/multipart/multipart.service';
import { GlobalFunctionService } from './../../services/global-function.service';
import { stateNameList, stateNameListHindi } from './../../state-list';

declare var $:any;

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
	
	signUpForm        : FormGroup;
	email 		      : FormControl;
	firstName         : FormControl;
	lastName          : FormControl;
	mobile            : FormControl;
	password  	      : FormControl;
	confPassCust      : FormControl;
	permanentAddress  : FormGroup;
	shopAddress       : FormGroup;
	realm 		      : FormControl;
	files             : any = {};
	
	errSuccObj : any = {};
	errMessages :any = {
		"Mobile number already exists":"मोबाइल नंबर पहले से मौजूद है",
		"email already exists":"ईमेल पहले से मौजूद है"
	}
	@Output() openOtpModal = new EventEmitter();
	
	constructor(@Inject(LOCALE_ID) protected localeId: string, private globalFunctionService : GlobalFunctionService, private multipartService : MultipartService) {
	}

	ngOnInit() {
	}
	
	openModal(){
		this.createForm();
		this.errSuccObj = {};
		$('#signUpModal').modal({backdrop: 'static', keyboard: false});
	}
	
	closeModal(){
		this.signUpForm.reset();
		this.errSuccObj = {};
		this.signUpForm.controls['realm'].setValue('webuser');
		$('#signUpModal').modal('hide');
	}
	
	createForm(){	
		// console.log('changed');
		this.firstName = new FormControl('',[
			Validators.required
		]);
		this.lastName = new FormControl('',[
			Validators.required
		]);
		this.email = new FormControl('',[
			Validators.required,
			validateEmail
		]);
		this.mobile = new FormControl('',[
			Validators.required,
			validateMobile
		]);
		this.password = new FormControl('',[
			Validators.required,
			Validators.minLength(6)
		]);
		this.confPassCust = new FormControl('',[
			Validators.required
		]);
		this.realm = new FormControl('webuser',[
			Validators.required
		]);
		this.signUpForm = new FormGroup({
			realm : this.realm,
			firstName : this.firstName,
			lastName : this.lastName,
			email : this.email,
			mobile : this.mobile,
			password : this.password,
			confPassCust : this.confPassCust
		});
	}

	onSubmit(){
		let _self = this;
		console.log("value : ", this.signUpForm.value);
		_self.errSuccObj = {};
		if(this.signUpForm.valid && this.signUpForm.value.password === this.signUpForm.value.confPassCust){
			
			console.log("form data : ", this.signUpForm.value);
			console.log("file : ", this.files);
			delete this.signUpForm.value.confPassCust;
			let data = { 'data' : this.signUpForm.value, 'files' : this.files };
			let _self = this;
			
			this.multipartService.signupApi(data).subscribe(
				(success)=>{
					console.log("sign up : ", success);
					_self.errSuccObj.success = this.localeId == "hi" ? "साइन अप सफलतापूर्वक, जारी रखने के लिए कृपया अपना ईमेल सत्यापित करें" : "Signed Up Successfully, Please verify your email to continue";
					_self.closeModal();
					_self.openOtpModal.emit({peopleId : success.success.data.id});
				},
				(error)=>{
					if(error === 'Server error'){
						_self.globalFunctionService.navigateToError('server');
					}else{
						if(this.localeId == "hi"){
							if(this.errMessages[error.message]){
								_self.errSuccObj.error = this.errMessages[error.message];
							}else{
								_self.errSuccObj.error = error.message;	
							}	
						}
						else{
							_self.errSuccObj.error = error.message;	
						}

					}
						
				}
			);
		}else{
			this.signUpForm.controls['mobile'].markAsTouched();
			this.signUpForm.controls['password'].markAsTouched();
			this.signUpForm.controls['confPassCust'].markAsTouched();
			this.signUpForm.controls['realm'].markAsTouched();
			this.signUpForm.controls['email'].markAsTouched();
			this.signUpForm.controls['firstName'].markAsTouched();
			this.signUpForm.controls['lastName'].markAsTouched();
		}
	}
}
