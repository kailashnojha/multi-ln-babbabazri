import { Component, OnInit, Output, EventEmitter, ViewChild , LOCALE_ID, Inject} from '@angular/core';
import { PeopleApi } from './../../sdk';

declare var $:any;


@Component({
  selector: 'app-rah-reset-password-request',
  templateUrl: './rah-reset-password-request.component.html',
  styleUrls: ['./rah-reset-password-request.component.css']
})
export class RahResetPasswordRequestComponent implements OnInit {

	user:any = {};
  actionResError:any = {
    isError:false,
    errMsg:""
  }
  errMessages :any = {
    "No user found":"खता नहीं मिला"
  }
  @Output() openResetOtpModal = new EventEmitter(); 
  @ViewChild("forgetForm") forgetForm;

	constructor(@Inject(LOCALE_ID) protected localeId: string, private peopleApi:PeopleApi) { 

	}

	ngOnInit() {

  }

	resetRequest(resForm){
    this.actionResError.isError = false;
    this.actionResError.msg = "";
    if(resForm.valid){
      let btn = $("#resetBtn");
      btn.button('loading');
      
	    this.peopleApi.resetRequest(this.user.mobile).subscribe((success)=>{
        console.log(success);
		    btn.button('reset');
        $('#rah-reset-password-request-modal').modal("hide");
        this.openResetOtpModal.emit({peopleId:success.success.data.id});
  		},(error)=>{
        btn.button('reset');
        this.actionResError.isError = true;
        if(this.localeId == "hi"){
          if(this.errMessages[error.message]){
            this.actionResError.msg = this.errMessages[error.message];
          }else{
            this.actionResError.msg = error.message;  
          }  
        }
        else{
          this.actionResError.msg = error.message;  
        }
        
        // this.actionResError.msg = error.message;
  		})
    }    
	}

  openModel(){
    this.actionResError.isError = false;
    this.actionResError.msg = "";
    this.forgetForm.resetForm();
    $('#rah-reset-password-request-modal').modal("show");
  }

  closeModel(){
    $('#rah-reset-password-request-modal').modal("hide");  
  }

}
