import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RahResetPasswordRequestComponent } from './rah-reset-password-request.component';

describe('RahResetPasswordRequestComponent', () => {
  let component: RahResetPasswordRequestComponent;
  let fixture: ComponentFixture<RahResetPasswordRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RahResetPasswordRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RahResetPasswordRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
