import { Component, OnInit, LOCALE_ID, Inject} from '@angular/core';
import { ReactiveFormsModule,FormsModule,FormGroup,FormControl,FormArray,Validators,FormBuilder } from '@angular/forms';

import { validateEmail } from './../../validators/email.validator';
import { validateMobile } from './../../validators/mobile.validator';

import { MultipartService } from './../../services/multipart/multipart.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.css']
})
export class ContactComponent implements OnInit {

	contactForm : FormGroup;
	name : FormControl;
	message : FormControl;
	contactNumber : FormControl;
	email : FormControl;
	errSuccesObj:any = {
		isError:false,
		isSuccess:false,
		errMsg:"",
		succMsg:""
	}
	mapAddresses: Array<any> = [
		{
			street: "Babba bazri bhawan, Behind jaika restorent, Gangapur Rd, Lalsot, Rajasthan 303503",
			mobile : "",
			"lat": 26.875940099999998,
	        "lng": 75.8837593
		},{
			street: "Plot No.42,R.R. Complex, Shop No.31, first floor, in front of India Gate, Tonk Road, Jaipur (Raj.)302022 India",
			mobile : "8949339872",
			"lat": 27.7273178,
	        "lng": 74.2718162
		},{
			street: "BABBA Bazri Bhawan, Near BSNL Office, Shivdaspura, Tehsil - Chaksu, Jaipur",
			mobile : "9460849995",
			"lat": 26.7093206,
	        "lng": 75.8914571
		},{
			street: "BABBA Bazri Bhawan, Near Jodhpur Sweets,  Nagar Palika Park, Kotputli Jaipur",
			mobile : "8955655002",
			"lat": 26.8949014,
	        "lng": 75.8337189
		},{
			street: "Shri Veer Tejaji Building Material, Riico Aria, Kaladera, Tehsil -Govindgarh, Jaipur",
			mobile : "",
			"lat": 26.9731943,
	        "lng": 75.6316327
		},{
			street: "BABBA Bazri Bhawan, Behind Jayaka Restorent, Ganagapur Road, Lalsot, Dausa",
			mobile : "6376425364",
			"lat": 26.548666,
	        "lng": 76.3397273
		},{
			street: "BABBA Bazri Bhawan Saras Dairy, (1895) Shivsinghpura Mode, Lalsot, Dausa",
			mobile : "9950333527",
			"lat": 26.5382747,
	        "lng": 76.2117952
		},{
			street: "Lokesh Janral Store,Gangadwadi, Tehsil-Sikrai, Dausa",
			mobile : "8509777077",
			"lat": 26.9162263,
	        "lng": 76.6845057
		},{
			street: "BABBA Bazri Bhawan, Tunga Road, Bichchha, Ramgarh Pachwara, Dausa",
			mobile : "9928401301",
			"lat": 26.7259181,
	        "lng": 76.0943718
		},{
			street: "Sanjay Hardwear, Nangal Rajawatan, Dausa",
			mobile : "",
			"lat": 26.8693958,
	        "lng": 75.7493994
		},{
			street: "BABBA Bazri Bhawan, Opp. Atal Sewa Kendra, Jhanoon, Tehsil- Bonli, Sawai Madhopur",
			mobile : "8769411879",
			"lat": 26.2560473,
	        "lng": 75.8366184
		},{
			street: "Sonu Janral Store, Bus Stand, Bamanwas, Sawai Madhopur",
			mobile : "9950944969",
			"lat": 26.5574636,
	        "lng": 76.5618966
		},{
			street: "Shivshakti Hotal, Near RTO Office NH12, Tonk",
			mobile : "9784097525",
			"lat": 26.7889531,
	        "lng": 75.8178131
		},{
			street: "BABBA Bazri Bhawan, Datwas Mode, Tehsil- Newai, Tonk",
			mobile : "9950381373",
			"lat": 26.4156042,
	        "lng": 75.9540308
		},{
			street: "BABBA Bazri Bhawan, Near Pani ki Tanki, Lawan, Dausa",
			mobile : "9461831064",
			"lat": 26.8043737,
	        "lng": 75.8692115
		}
	];


	selectedAddress : any = {
		street: "Babba bazri bhawan, Behind jaika restorent, Gangapur Rd, Lalsot, Rajasthan 303503",
		mobile : "",
		"lat": 26.547721,
        "lng": 76.341898
	}		

	constructor(@Inject(LOCALE_ID) protected localeId: string, private multipartService : MultipartService) { 
		this.createForm();
	}

	ngOnInit() {
	}

	setAddress(address){
		this.selectedAddress = address;
	}

	createForm(){
		this.email = new FormControl('',[
			Validators.required,
			validateEmail
		]);
		this.name = new FormControl('',[
			Validators.required
		]);
		this.message = new FormControl('',[
			Validators.required
		]);
		this.contactNumber = new FormControl('',[
			Validators.required,
			validateMobile
		]);
		this.contactForm = new FormGroup({
			email: this.email,
			name : this.name,
			message : this.message,
			contactNumber : this.contactNumber
		});
	}
	
	onSubmit(){

		this.errSuccesObj = {
			isError:false,
			isSuccess:false,
			errMsg:"",
			succMsg:""
		}

		let _self = this;
		if(this.contactForm.valid){
			this.multipartService.contactUs(this.contactForm.value.name,this.contactForm.value.email,this.contactForm.value.contactNumber,this.contactForm.value.message).subscribe(
				(success)=>{
					// console.log("data added : ", success);
					_self.contactForm.reset();
					this.errSuccesObj = {
						isError:false,
						isSuccess:true,
						errMsg:"",
						succMsg: this.localeId == 'hi'? "सफलतापूर्वक इन्क्वारी  भेजी गई":"Successfully sent enquiry"
					}
					// alert('Enquiry Sent');
				},
				(error)=>{
					// console.log("error : ", error);
					this.errSuccesObj = {
						isError:true,
						isSuccess:false,
						errMsg:error.message,
						succMsg:""
					}
					// alert(error);
				}
			);
		}else{
			this.contactForm.controls['email'].markAsTouched();
			this.contactForm.controls['name'].markAsTouched();
			this.contactForm.controls['message'].markAsTouched();
			this.contactForm.controls['contactNumber'].markAsTouched();
		}
	}
}
