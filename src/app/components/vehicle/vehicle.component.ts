import { Component, OnInit, AfterViewInit,  LOCALE_ID, Inject  } from '@angular/core';
import { ReactiveFormsModule,FormsModule,FormGroup,FormControl,FormArray,Validators,FormBuilder } from '@angular/forms';
import { MultipartService } from './../../services/multipart/multipart.service';
import { GlobalFunctionService } from './../../services/global-function.service';
import { validateEmail } from './../../validators/email.validator';
import { validateMobile } from './../../validators/mobile.validator';
import { validatePercentage } from './../../validators/percentage.validator';
import { validateNoOfEngineers } from './../../validators/numberOfEng.validator';
import { validateAccountNo } from './../../validators/accountNo.validator';
import { Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { stateNameList, stateNameListHindi } from './../../state-list';
declare var $: any;

@Component({
  selector: 'app-vehicle',
  templateUrl: './vehicle.component.html',
  styleUrls: ['./vehicle.component.css']
})
export class VehicleComponent implements OnInit {

	vehicleForm : FormGroup;
	email : FormControl;
	ownerOrFirm : FormControl;
	fatherName : FormControl;
	district : FormControl;
	tehsil : FormControl;
	state : FormControl;
	vehicleType : FormControl;
	model : FormControl;
	rcNo : FormControl;
	contactNo : FormControl;
	whatsAppNo : FormControl;
	date : FormControl;
	
	files : any = {};
	vehicleTypes : any;
	
	noSign : boolean = false;
	noVehicleImg : boolean = false;
	stateList : any = [];
	stateListHindi:any = {};
	errSuccObj : any = {};
	isForm : boolean = false;
	myInfo : any = {};
	disabled : boolean = false;
	hide : boolean = true;
	
	constructor(@Inject(LOCALE_ID) protected localeId: string, private router : Router, private globalFunctionService : GlobalFunctionService, private multipartService : MultipartService) {
		this.stateList = stateNameList;
		this.stateListHindi = stateNameListHindi;
	}

	ngOnInit(){
		this.errSuccObj = {};
		this.getMyInfo();
	}
	
	getMyInfo(){
		let _self = this;
		this.isForm = true;
		this.multipartService.getMyInfo().subscribe(
			(success)=>{
				console.log("my info : ", success);
				_self.myInfo = success.success.data;
				if(_self.myInfo.realm === 'webuser'){
					if(!_self.myInfo.vehicleDone){
						this.createForm();
						this.getVehicleTypes();
					}else{
						_self.isForm = false;
					}
				}else{
					this.createForm();
					this.getVehicleTypes();
				}
			},
			(error)=>{
				console.log("error : ", error);
			}
		);
	}
	
	ngAfterViewInit(){
	}
	
	createForm(){
		this.email = new FormControl('',[
			Validators.required,
			validateEmail
		]);
		this.ownerOrFirm = new FormControl('',[
			Validators.required
		]);
		this.fatherName = new FormControl('',[
			Validators.required
		]);
		this.district = new FormControl('',[
			Validators.required
		]);
		this.tehsil = new FormControl('',[
			Validators.required
		]);
		this.state = new FormControl(this.stateList[0],[
			Validators.required
		]);
		this.vehicleType = new FormControl('',[
			Validators.required
		]);
		this.model = new FormControl('',[
			Validators.required
		]);
		this.rcNo = new FormControl('',[
			Validators.required
		]);
		this.contactNo = new FormControl('',[
			Validators.required,
			validateMobile
		]);
		this.whatsAppNo = new FormControl('',[
			validateMobile
		]);
		this.date = new FormControl('',[
		]);
		this.vehicleForm = new FormGroup({
			email : this.email,
			ownerOrFirm : this.ownerOrFirm,
			fatherName : this.fatherName,
			district : this.district,
			tehsil : this.tehsil,
			state : this.state,
			vehicleType : this.vehicleType,
			model : this.model,
			rcNo : this.rcNo,
			contactNo : this.contactNo,
			whatsAppNo : this.whatsAppNo,
			date : this.date
		});
	}
	
	getVehicleTypes(){
		let _self = this;
		this.multipartService.getVehicleType().subscribe(
			(success)=>{
				console.log("vehicles : ", success);
				_self.vehicleTypes = success.success.data;
				_self.vehicleForm.controls['vehicleType'].setValue(_self.vehicleTypes[0].name);
			},
			(error)=>{
				console.log("error : ", error);
				if(error === 'Server error'){
					_self.globalFunctionService.navigateToError('server');
				}
			}
		);
	}
	
	resetForm(){
		this.vehicleForm.reset();
		this.vehicleForm.controls['vehicleType'].setValue(this.vehicleTypes[0]);
		this.vehicleForm.controls['state'].setValue(this.stateList[0]);
		this.files = {};
		setTimeout(()=>{
			if(!this.hide){
				this.hide = true;
			}
		},10000);
	}
	
	onSubmit(){
		this.errSuccObj = {};
		/*console.log("form data : ", this.vehicleForm.value);
		console.log("file : ", this.files);*/
		let data = { 'data' : JSON.parse(JSON.stringify(this.vehicleForm.value)), 'files' : this.files };
		let _self = this;
		if(this.vehicleForm.valid && this.files.signImg && this.files.vehicleImg){
			this.disabled = true;

			this.setHiValue(data.data);	
			// return;
			this.multipartService.vehicleApi(data).subscribe(
				(success)=>{
					
					_self.errSuccObj.success = this.localeId == "hi" ? "डेटा सफलतापूर्वक जोड़ा गया" : "data added successfully";
					_self.disabled = false;
					_self.hide = false;
					_self.resetForm();
					_self.router.navigate(['dashboard']);
				},
				(error)=>{
					_self.disabled = false;
					console.log("error : ", error);
					if(error === 'Server error'){
						_self.globalFunctionService.navigateToError('server');
					}
					else if(error.statusCode === 401){
						_self.globalFunctionService.navigateToError('401');
					}
					else{	
						_self.errSuccObj.error = error.message;
					}
				}
			);
		}else{
			this.vehicleForm.controls['email'].markAsTouched();
			this.vehicleForm.controls['ownerOrFirm'].markAsTouched();
			this.vehicleForm.controls['fatherName'].markAsTouched();
			this.vehicleForm.controls['district'].markAsTouched();
			this.vehicleForm.controls['tehsil'].markAsTouched();
			this.vehicleForm.controls['state'].markAsTouched();
			this.vehicleForm.controls['model'].markAsTouched();
			this.vehicleForm.controls['rcNo'].markAsTouched();
			this.vehicleForm.controls['contactNo'].markAsTouched();
			
			if(!this.files.signImg){
				this.noSign = true;
			}
			if(!this.files.vehicleImg){
				this.noVehicleImg = true;
			}
		}
	}

	setHiValue(data){
		
		console.log(data);
		if(this.localeId != "hi")
			return;

		if(data.state){
			data.state = this.stateListHindi[data.state];
		}
	}


}
