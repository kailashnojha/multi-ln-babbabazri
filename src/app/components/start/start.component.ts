import { Component, OnInit, AfterViewInit, ViewChild, NgZone,EventEmitter, Output, ElementRef, ContentChild  } from '@angular/core';
import { ReactiveFormsModule,FormsModule,FormGroup,FormControl,FormArray,Validators,FormBuilder } from '@angular/forms';
import { PeopleApi,LoopBackConfig,LoopBackAuth } from './../../sdk/index';
import { GlobalFunctionService } from './../../services/global-function.service';

declare var $:any;

@Component({
	selector: 'app-start',
	templateUrl: './start.component.html',
	styleUrls: ['./start.component.css']
})
export class StartComponent implements OnInit {

	baseUrl : String;
	authCred : any = {};
	isLogin : boolean = false;

	constructor(private globalFunctionService : GlobalFunctionService,
		private peopleApi : PeopleApi, private auth : LoopBackAuth) {
		//console.log("bank name : ", this.bankNames);
		this.baseUrl = LoopBackConfig.getPath();
	}

	ngOnInit(){
		this.setAuthCredential();
	}

	setAuthCredential(){
		this.authCred.tokenId = this.auth.getAccessTokenId();
		this.authCred.userId = this.auth.getCurrentUserId();
		if(this.authCred.tokenId && this.authCred.userId){
			this.isLogin = true;
		}
		else{
			this.isLogin = false;
		}
	}
}
