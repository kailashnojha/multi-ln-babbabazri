import { Component, OnInit, Output, EventEmitter } from '@angular/core';

import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, FormArray, Validators, FormBuilder } from '@angular/forms';
import { MultipartService } from './../../services/multipart/multipart.service';
import { validateEmail } from './../../validators/email.validator';
import { GlobalFunctionService } from './../../services/global-function.service';
import { PeopleApi, LoopBackAuth } from './../../sdk/index';
import { SDKToken } from './../../sdk/models/BaseModels';
import { Router } from '@angular/router';

declare var $:any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
	
	loginForm : FormGroup;
	realm : FormControl;
	email : FormControl;
	password : FormControl;
	errSuccObj : any = {};
	
	@Output() loginDone = new EventEmitter(); 
	@Output() resendOtp = new EventEmitter();
	@Output() reset = new EventEmitter();
	
	constructor(private router : Router, private globalFunctionService : GlobalFunctionService, private multipartService : MultipartService, private peopleApi : PeopleApi, private auth : LoopBackAuth) {}

	ngOnInit() {
	}
	
	openModal(){
		this.createForm();
		this.errSuccObj = {};
		$('#myModal').modal('show');
	}
	
	closeModal(){
		this.loginForm.reset();
		this.errSuccObj = {};
		this.loginForm.controls['realm'].setValue('webuser');
		$('#myModal').modal('hide');
	}
	
	createForm(){
		this.email = new FormControl('',[
			Validators.required,
			validateEmail
		]);
		this.password = new FormControl('',[
			Validators.required
		]);
		this.realm = new FormControl('webuser',[
			Validators.required
		]);
		this.loginForm = new FormGroup({
			realm : this.realm,
			email : this.email,
			password : this.password
		});
	}

	onSubmit(){
		let _self = this;
		console.log("value : ", this.loginForm.value);
		this.errSuccObj = {};
		if(this.loginForm.valid){
			this.peopleApi.login(this.loginForm.value,'user',false).subscribe(
				(success)=>{
					console.log("login : ", success);
					_self.closeModal();
					let token: SDKToken = new SDKToken();
					token = {
						'id' : success.success.data.access_token,
						'user' : JSON.stringify(success.success.data.user),
						'userId' : success.success.data.user.id,
						'created' : success.success.data.created,
						'ttl' : success.success.data.ttl,
						'rememberMe' : false,
						'scopes'	 : ''
					};
					_self.auth.setToken(token);
					_self.router.navigate(['dashboard']);
					_self.loginDone.emit();
				},
				(error)=>{
					console.log(error);
					if(error.statusCode === 401){
						if(error.code === "LOGIN_ACCOUNT_VERIFIED_FAILED"){
							_self.closeModal();//error.details.userId
							_self.resendOtp.emit({peopleId : error.details.userId,type : 'webuserSignup'});
						}else{
							if(error === 'Server error'){
								_self.closeModal();
								_self.globalFunctionService.navigateToError('server');
							}else{
								_self.errSuccObj.error = error.message;
							}
						}
					}
					if(error === 'Server error'){
						_self.globalFunctionService.navigateToError('server');
					}
				}
			);
		}else{
			this.loginForm.controls['email'].markAsTouched();
			this.loginForm.controls['password'].markAsTouched();
			this.loginForm.controls['realm'].markAsTouched();
		}
	}

	resetPassword(){
		this.reset.emit();
	}

}
