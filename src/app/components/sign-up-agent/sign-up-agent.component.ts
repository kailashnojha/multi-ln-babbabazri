import { Component, OnInit, Output, EventEmitter , LOCALE_ID, Inject} from '@angular/core';
import { ReactiveFormsModule,FormsModule,FormGroup,FormControl,FormArray,Validators,FormBuilder } from '@angular/forms';

import { validateEmail } from './../../validators/email.validator';
import { validateMobile } from './../../validators/mobile.validator';

import { MultipartService } from './../../services/multipart/multipart.service';
import { GlobalFunctionService } from './../../services/global-function.service';
import { stateNameList,stateNameListHindi } from './../../state-list';

declare var $ : any;
@Component({
  selector: 'app-sign-up-agent',
  templateUrl: './sign-up-agent.component.html',
  styleUrls: ['./sign-up-agent.component.css']
})
export class SignUpAgentComponent implements OnInit {

	signUpForm    : FormGroup;
	email 		  : FormControl;
	firstName     : FormControl;
	lastName      : FormControl;
	mobile        : FormControl;
	password  	  : FormControl;
	confPassCust  : FormControl;
	permanentAddress : FormGroup;
	shopAddress   : FormGroup;
	realm 		  : FormControl;
	
	city : FormControl;
	tehsil : FormControl;
	district : FormControl;
	state : FormControl;
	value : FormControl
	
	city1 : FormControl;
	tehsil1 : FormControl;
	district1 : FormControl;
	state1 : FormControl;
	value1 : FormControl
	
	files : any = {};
	noAadhar : boolean = false;
	errSuccObj : any = {};
	
	stateList : any = [];
	stateHindiList:any = [];
	seletectedLanguage:string ="en";
	@Output() openOtpModal = new EventEmitter();
	errMessages :any = {
		"Mobile number already exists":"मोबाइल नंबर पहले से मौजूद है",
		"email already exists":"ईमेल पहले से मौजूद है"
	}
	constructor(@Inject(LOCALE_ID) protected localeId: string, private globalFunctionService : GlobalFunctionService, private multipartService : MultipartService) {
		this.stateList = stateNameList;
		this.stateHindiList = stateNameListHindi;
		this.seletectedLanguage = this.localeId;
		
	}

	ngOnInit() {
	}
  
	openModal(){
		this.createForm();
		this.errSuccObj = {};
		$('#signUpAgentModal').modal({backdrop: 'static', keyboard: false});
	}
	
	closeModal(){
		this.signUpForm.reset();
		this.files = {};
		this.signUpForm.controls['realm'].setValue('emitra');
		$('#signUpAgentModal').modal('hide');
	}
	
	createForm(){	
		console.log('changed');
		this.firstName = new FormControl('',[
			Validators.required
		]);
		this.lastName = new FormControl('',[
			Validators.required
		]);
		this.email = new FormControl('',[
			Validators.required,
			validateEmail
		]);
		this.mobile = new FormControl('',[
			Validators.required,
			validateMobile
		]);
		this.password = new FormControl('',[
			Validators.required,
			Validators.minLength(6)
		]);
		this.confPassCust = new FormControl('',[
			Validators.required
		]);
		
		this.value = new FormControl('',[
			Validators.required
		]);
		this.city = new FormControl('',[
			Validators.required
		]);
		this.tehsil = new FormControl('',[
			Validators.required
		]);
		this.district = new FormControl('',[
			Validators.required
		]);
		this.state = new FormControl(this.stateList[0],[
			Validators.required
		]);
		
		this.value1 = new FormControl('',[
			Validators.required
		]);
		this.city1 = new FormControl('',[
			Validators.required
		]);
		this.tehsil1 = new FormControl('',[
			Validators.required
		]);
		this.district1 = new FormControl('',[
			Validators.required
		]);
		this.state1 = new FormControl(this.stateList[0],[
			Validators.required
		]);
		
		this.permanentAddress = new FormGroup({
			value : this.value,
			city : this.city,
			tehsil : this.tehsil,
			district : this.district,
			state : this.state
		});
		this.shopAddress = new FormGroup({
			value1 : this.value1,
			city1 : this.city1,
			tehsil1 : this.tehsil1,
			district1 : this.district1,
			state1 : this.state1
		});
	
		this.realm = new FormControl('emitra',[
			Validators.required
		]);
		this.signUpForm = new FormGroup({
			realm : this.realm,
			firstName : this.firstName,
			lastName : this.lastName,
			email : this.email,
			mobile : this.mobile,
			password : this.password,
			confPassCust : this.confPassCust,
			permanentAddress : this.permanentAddress,
			shopAddress : this.shopAddress
		});
		
		console.log('form : ', this.signUpForm.value );
	}

	onSubmit(){
		let _self = this;
		console.log("value : ", this.signUpForm.value);
		_self.errSuccObj = {};
		if(this.signUpForm.valid && this.files.aadharImg && this.signUpForm.value.password === this.signUpForm.value.confPassCust){
			
			console.log("form data : ", this.signUpForm.value);
			console.log("file : ", this.files);
			delete this.signUpForm.value.confPassCust;
			let data = { 'data' : this.signUpForm.value, 'files' : this.files };
			let _self = this;
			
			this.multipartService.signupApi(data).subscribe(
				(success)=>{
					console.log("sign up : ", success);
					_self.errSuccObj.success = this.localeId == "hi" ? "साइन अप सफलतापूर्वक, जारी रखने के लिए कृपया अपना ईमेल सत्यापित करें" : "Signed Up Successfully, Please verify your email to continue";
					_self.closeModal();
					_self.openOtpModal.emit({peopleId : success.success.data.id});
				},
				(error)=>{
					console.log("error : ", error);
					if(error === 'Server error'){
						_self.globalFunctionService.navigateToError('server');
					}else{
						// _self.errSuccObj.error = error.message;
						if(this.localeId == "hi"){
							if(this.errMessages[error.message]){
								_self.errSuccObj.error = this.errMessages[error.message];
							}else{
								_self.errSuccObj.error = error.message;	
							}	
						}
						else{
							_self.errSuccObj.error = error.message;	
						}
					}
				}
			);
		}else{
			this.signUpForm.controls['mobile'].markAsTouched();
			this.signUpForm.controls['password'].markAsTouched();
			this.signUpForm.controls['confPassCust'].markAsTouched();
			this.signUpForm.controls['realm'].markAsTouched();
			this.signUpForm.controls['email'].markAsTouched();
			this.signUpForm.controls['firstName'].markAsTouched();
			this.signUpForm.controls['lastName'].markAsTouched();
			this.permanentAddress.controls['value'].markAsTouched();
			this.permanentAddress.controls['district'].markAsTouched();
			this.permanentAddress.controls['city'].markAsTouched();
			this.permanentAddress.controls['tehsil'].markAsTouched();
			
			this.shopAddress.controls['value1'].markAsTouched();
			this.shopAddress.controls['district1'].markAsTouched();
			this.shopAddress.controls['city1'].markAsTouched();
			this.shopAddress.controls['tehsil1'].markAsTouched();
			
			if(!this.files.aadharImg)
			this.noAadhar = true;
		}
	}
}
