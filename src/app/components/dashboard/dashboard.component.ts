import { Component, OnInit } from '@angular/core';
import { LoopBackConfig,LoopBackAuth,PeopleApi } from './../../sdk/index';
import { GlobalFunctionService } from './../../services/global-function.service';

declare var $ : any;
@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

	myInfo : any = {};
	loading : boolean = true;
	vehicles : any = [];
	employments : any = [];
	dealerships : any = [];
	baseUrl : String;
	loadingForm : boolean = true;
	formType : String = '';
	
	constructor(private peopleApi : PeopleApi, private globalFunctionService : GlobalFunctionService) {
		this.baseUrl = LoopBackConfig.getPath();
	}

	ngOnInit() {
		this.getMyInfo();
	}
	
	print(id){
		var prtContent = document.getElementById(id);
		var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0');
		setTimeout(()=>{
			WinPrint.document.write(prtContent.innerHTML);	
		})
		
		setTimeout(()=>{
			
			WinPrint.document.close();
			WinPrint.focus();
			WinPrint.print();
			WinPrint.close();	
		},3000)
		
	}
	
	getAllRequest(type){
		let _self = this;
		this.dealerships = [];
		this.employments = [];
		this.vehicles = [];
		this.loadingForm = true;
		this.peopleApi.getAllRequests().subscribe(
			(success)=>{
				console.log("requests : ", success.success.data);
				if(type === 'dealership' || type === ''){
					for(var i=0;i<=success.success.data.dealerships.length -1;i++){
						if(success.success.data.dealerships[i].shopImg){
							success.success.data.dealerships[i].shopImg = _self.baseUrl + success.success.data.dealerships[i].shopImg;
						}
					}
					_self.dealerships = success.success.data.dealerships;
				}
				if(type === 'employment' || type === '' ){
					for(var i=0;i<=success.success.data.employments.length -1;i++){
						if(success.success.data.employments[i].signImg){
							success.success.data.employments[i].signImg = _self.baseUrl + success.success.data.employments[i].signImg;
						}
						if(success.success.data.employments[i].profilePic){
							success.success.data.employments[i].profilePic = _self.baseUrl + success.success.data.employments[i].profilePic;
						}
					}
					_self.employments = success.success.data.employments;
				}
				if(type === 'vehicle' || type === '' ){
					for(var i=0;i<=success.success.data.vehicles.length -1;i++){
						if(success.success.data.vehicles[i].signImg){
							success.success.data.vehicles[i].signImg = _self.baseUrl + success.success.data.vehicles[i].signImg;
						}
						if(success.success.data.vehicles[i].vehicleImg){
							success.success.data.vehicles[i].vehicleImg = _self.baseUrl + success.success.data.vehicles[i].vehicleImg;
						}
					}
					_self.vehicles = success.success.data.vehicles;
				}
				setTimeout(()=>{
					_self.loadingForm = false;
				},1);
			},
			(error)=>{
				if(error === 'Server error'){
					_self.globalFunctionService.navigateToError('server');
				}else if(error.statusCode === 401){
					_self.globalFunctionService.navigateToError('401');
				}else{
					console.log('error : ', error);
					//_self.globalFunctionService.errorToast(error.message,'oops');
				}
				console.log("error : ", error);
			}
		);
	}
  
	getMyInfo(){
		let _self = this;
		this.peopleApi.getMyInfo().subscribe(
			(success)=>{
				console.log("my info : ", success.success.data);
				_self.myInfo = success.success.data;
				if(_self.myInfo.realm === 'webuser'){
					if(_self.myInfo.employmentDone || _self.myInfo.dealershipDone || _self.myInfo.vehicleDone){
						_self.getAllRequest('');
					}else{
						
					}
				}else{
					_self.formType = 'dealership';
					_self.getAllRequest(_self.formType);
				}
			},
			(error)=>{
				if(error === 'Server error'){
					_self.globalFunctionService.navigateToError('server');
				}else if(error.statusCode === 401){
					_self.globalFunctionService.navigateToError('401');
				}else{
					console.log('error : ', error);
					//_self.globalFunctionService.errorToast(error.message,'oops');
				}
				console.log("error : ", error);
			}
		);
	}
	
	checkCont(evt){
		if($(evt) && $(evt)[0]){
			if($(evt)[0].style.display === 'block'){
				$(evt)[0].style.display  = 'none';
				$(evt).siblings().removeClass('active-row');
			}else{
				$(evt)[0].style.display = 'block';
				$(evt).siblings().addClass('active-row');
			}
		}
	}
}