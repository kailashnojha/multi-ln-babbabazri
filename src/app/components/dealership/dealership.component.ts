import { Component, OnInit,LOCALE_ID, Inject } from '@angular/core';
import { ReactiveFormsModule,FormsModule,FormGroup,FormControl,FormArray,Validators,FormBuilder } from '@angular/forms';
import { GlobalFunctionService } from './../../services/global-function.service';
import { validateEmail } from './../../validators/email.validator';
import { validateMobile } from './../../validators/mobile.validator';
import { validateWorkEx } from './../../validators/yearOfExp.validator';
import { validateNoOfEngineers } from './../../validators/numberOfEng.validator';
import { validateAccountNo } from './../../validators/accountNo.validator';

import { MultipartService } from './../../services/multipart/multipart.service';
import { Router } from '@angular/router';
import { stateNameList, stateNameListHindi } from './../../state-list';

@Component({
  selector: 'app-dealership',
  templateUrl: './dealership.component.html',
  styleUrls: ['./dealership.component.css']
})
export class DealershipComponent implements OnInit {
	// @Inject(LOCALE_ID) public locale: string
	bussType : any = [];
	stateList : any = [];
	stateListHiEn : any = {};
	currentBussTypeHiEn: any = {};
	individual : boolean = false;
	toShowInput : boolean = false;
	
	dealershipForm : FormGroup;
	
	email : FormControl;
	nameOrFirm : FormControl;
	fatherName : FormControl;
	contactNo : FormControl;
	postalAddress : FormGroup;
	value : FormControl;
	district : FormControl;
	tehsil : FormControl;
	city : FormControl;
	state : FormControl;
	
	value1 : FormControl;
	district1 : FormControl;
	tehsil1 : FormControl;
	city1 : FormControl;
	state1 : FormControl;
	
	shopAddress : FormGroup;
	turnover : FormControl;
	otherDetail : FormControl;
	other : FormControl;
	currentBussType : FormControl;
	gstNo : FormControl;
	investAmount : FormControl;
	
	files   : any = {};
	errSuccObj : any = {};
	isForm : boolean = false;
	myInfo : any = {};
	hide : boolean = true;
	disabled : boolean = false;
	constructor(@Inject(LOCALE_ID) protected localeId: string, private router : Router,private globalFunctionService : GlobalFunctionService, private multipartService : MultipartService){
		this.stateList = stateNameList;
		this.stateListHiEn = stateNameListHindi;
		console.log("local_id",this.localeId)
	}
	
	createForm(){
		
		if(this.myInfo.realm === 'webuser'){
			this.email = new FormControl(this.myInfo.email,[
				Validators.required,
				validateEmail
			]);
		}else{
			this.email = new FormControl('',[
				Validators.required,
				validateEmail
			]);
		}
		this.nameOrFirm = new FormControl('',[
			Validators.required
		]);
		this.fatherName = new FormControl('',[
			Validators.required
		]);
		this.contactNo = new FormControl('',[
			Validators.required,
			validateMobile
		]);
		this.value = new FormControl('',[
			Validators.required
		]);
		this.district = new FormControl('',[
			Validators.required
		]);
		this.city = new FormControl('',[
			Validators.required
		]);
		this.tehsil = new FormControl('',[
			Validators.required
		]);
		this.state = new FormControl(stateNameList[0],[
			Validators.required
		]);
		
		this.value1 = new FormControl('',[
			Validators.required
		]);
		this.district1 = new FormControl('',[
			Validators.required
		]);
		this.city1 = new FormControl('',[
			Validators.required
		]);
		this.tehsil1 = new FormControl('',[
			Validators.required
		]);
		this.state1 = new FormControl(stateNameList[0],[
			Validators.required
		]);
		
		this.postalAddress = new FormGroup({
			value : this.value,
			district : this.district,
			city : this.city,
			tehsil : this.tehsil,
			state : this.state
		});
		this.shopAddress = new FormGroup({
			value1 : this.value1,
			district1 : this.district1,
			city1 : this.city1,
			tehsil1 : this.tehsil1,
			state1 : this.state1
		});
		this.turnover = new FormControl('',[
			validateWorkEx
		]);
		this.otherDetail = new FormControl('',[]);
		this.other = new FormControl('',[]);
		this.currentBussType = new FormControl('',[]);
		this.gstNo = new FormControl('',[]);
		this.investAmount = new FormControl('',[
			validateWorkEx
		]);
		
		this.dealershipForm = new FormGroup({
			email : this.email,
			nameOrFirm : this.nameOrFirm,
			fatherName : this.fatherName,
			contactNo : this.contactNo,
			postalAddress : this.postalAddress,
			shopAddress : this.shopAddress,
			turnover : this.turnover,
			otherDetail : this.otherDetail,
			other : this.other,
			currentBussType : this.currentBussType,
			gstNo : this.gstNo,
			investAmount : this.investAmount
		});
	}
	
	ngOnInit(){

		this.currentBussTypeHiEn = {
			"CEMENT BUSINESS"                      : "सीमेंट बिज़नेस",
			"STEEL BAR SHOP"                       : "स्टील बार शॉप",
			"BRICKS/ RODI/GITTI"                   : "ब्रिक्स/ रोड़ी/गिट्टी",
			"BUILDING CONTRACTOR/ROAD CONTRACTOR"  : "बिल्डिंग कांट्रेक्टर/रोड कांट्रेक्टर",
			"OTHER"                                : "अन्य"
		}

		this.errSuccObj = {};
		this.getMyInfo();
	}
	
	getMyInfo(){
		let _self = this;
		this.isForm = true;
		this.multipartService.getMyInfo().subscribe(
			(success)=>{
				console.log("my info : ", success);
				_self.myInfo = success.success.data;
				if(_self.myInfo.realm === 'webuser'){
					if(!_self.myInfo.dealershipDone){
						_self.createForm();
					}else{
						_self.isForm = false;
					}
				}else{
					_self.createForm();
				}
			},
			(error)=>{
				console.log("error : ", error);
			}
		);
	}
	
	onSubmit(){
		// console.log("form data : ", this.dealershipForm.value);
		this.errSuccObj = {};
		if(this.dealershipForm.valid && this.postalAddress.valid && this.shopAddress.valid){
			this.disabled = true;
			/*console.log("form data : ", this.dealershipForm.value);
			console.log("other : ", this.dealershipForm.value.other);*/
			if(this.toShowInput){
				this.bussType.push(this.dealershipForm.value.other);
			}
			let obj1 = { 'bussType' : this.bussType };
			// console.log("buss type : ", obj1);
			delete this.dealershipForm.value.currentBussType;
			delete this.dealershipForm.value.other;
			let obj3 = { 'shopAddress' : {
				'value' : this.shopAddress.value.value1,
				'district' : this.shopAddress.value.district1,
				'city' : this.shopAddress.value.city1,
				'tehsil' : this.shopAddress.value.tehsil1,
				'state' : this.shopAddress.value.state1
			}};
			
			delete this.shopAddress.value.value1;
			delete this.shopAddress.value.district1;
			delete this.shopAddress.value.city1;
			delete this.shopAddress.value.tehsil1;
			delete this.shopAddress.value.state1;
			delete this.dealershipForm.value.shopAddress;
			let obj2 = Object.assign({},obj1, obj3, this.dealershipForm.value);
			let data = { 'data' : obj2, 'files' : this.files };
			let _self = this;
			
			this.setHiValue(data.data);

			// return;

			this.multipartService.dealershipApi(data).subscribe(
				(success)=>{
					console.log("data added : ", success);
					_self.errSuccObj.success = this.localeId == "hi" ? "डेटा सफलतापूर्वक जोड़ा गया" : "Data Added Successfully";
					_self.hide = false;
					_self.disabled = false;
					_self.resetForm();
					_self.router.navigate(['dashboard']);
				},
				(error)=>{
					_self.disabled = false;
					console.log("error : ", error);
					if(error === 'Server error'){
						_self.globalFunctionService.navigateToError('server');
					}else if(error.statusCode === 401){
						_self.globalFunctionService.navigateToError('401');
					}else{
						_self.errSuccObj.error = error.message;
					}
				}
			);	
		}else{
			this.dealershipForm.controls['email'].markAsTouched();
			this.dealershipForm.controls['nameOrFirm'].markAsTouched();
			this.dealershipForm.controls['fatherName'].markAsTouched();
			this.dealershipForm.controls['contactNo'].markAsTouched();
			this.postalAddress.controls['value'].markAsTouched();
			this.postalAddress.controls['district'].markAsTouched();
			this.postalAddress.controls['city'].markAsTouched();
			this.postalAddress.controls['tehsil'].markAsTouched();
			this.postalAddress.controls['state'].markAsTouched();
			this.shopAddress.controls['value1'].markAsTouched();
			this.shopAddress.controls['district1'].markAsTouched();
			this.shopAddress.controls['city1'].markAsTouched();
			this.shopAddress.controls['tehsil1'].markAsTouched();
			this.shopAddress.controls['state1'].markAsTouched();
		}
	}
	

	setHiValue(data){

		// console.log(data);

		if(this.localeId != "hi")
			return;

		let bussType = [];
		for(let i=0;i<data.bussType.length;i++){
			bussType.push(this.currentBussTypeHiEn[data.bussType[i]]);
		}

		data.bussType = bussType;

		if(data.postalAddress && data.postalAddress.state){
			data.postalAddress.state = this.stateListHiEn[data.postalAddress.state];
		}

		if(data.shopAddress && data.shopAddress.state){
			data.shopAddress.state = this.stateListHiEn[data.shopAddress.state];
		}



	}




	resetForm(){
		this.dealershipForm.reset();
		this.postalAddress.controls['state'].setValue(this.stateList[0]);
		this.shopAddress.controls['state1'].setValue(this.stateList[0]);
		this.postalAddress.reset();
		this.shopAddress.reset();
		this.files = {};
		this.bussType = [];
		var inputs=document.getElementsByTagName("input");
		for (var i in inputs)
			if (inputs[i].type=="checkbox") inputs[i].checked=false;
		
		setTimeout(()=>{
			if(!this.hide){
				this.hide = true;
			}
		},10000);
	}
	
	changeType(type,evt){
		let newValue;
		if(evt.target.checked){
			if(type === 'OTHER'){
				this.toShowInput = true;
			}else{
				this.bussType.push(type);
				if(this.dealershipForm.controls['currentBussType'].value){
					newValue = this.dealershipForm.controls['currentBussType'].value +  ',' + type;
				}else{
					newValue = type;
				}
				this.dealershipForm.controls['currentBussType'].setValue(newValue);
			}
		}
		else{
			if(type === 'OTHER'){
				this.toShowInput = false;
			}
			else{
				let newArr = this.dealershipForm.controls['currentBussType'].value.split(',');
				const index = newArr.indexOf(type);
				console.log("index : ", index);
				if(index !== -1){
					newArr.splice(index,1);
					this.bussType.splice(index,1);
				}
				newValue = newArr.join(',');
				this.dealershipForm.controls['currentBussType'].setValue(newValue);
			}
		}
		//console.log("problems array : ", this.bussType);
		//console.log("problems Ids : ", this.dealershipForm.controls['currentBussType'].value);
	}
}
