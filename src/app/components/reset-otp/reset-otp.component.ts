import { Component, OnInit , Output, EventEmitter, ViewChild, LOCALE_ID, Inject} from '@angular/core';
import { PeopleApi } from './../../sdk';

declare var $:any;

@Component({
  selector: 'app-reset-otp',
  templateUrl: './reset-otp.component.html',
  styleUrls: ['./reset-otp.component.css']
})
export class ResetOtpComponent implements OnInit {
  
  peopleId:string = "";
  errSuccObj : any = {};
  otp:string;
  firebaseToken:string = "";
  @ViewChild("resOtpForm") resOtpForm;
  @Output() openCPModal = new EventEmitter();
  	errMessages :any = {
		"Otp doesn't match":"ओटीपी मेल नहीं खाता है"
	}
  constructor(@Inject(LOCALE_ID) protected localeId: string, private peopleApi:PeopleApi) { }

	ngOnInit() {
	}

	resendOtp(peopleId,type){
		let _self = this;
		this.peopleApi.resendOtp(this.peopleId,type).subscribe(
			(success)=>{
				_self.errSuccObj.success = this.localeId == "hi" ? "नया ओटीपी आपके पंजीकृत मोबाइल नंबर पर भेजा गया है" : 'New Otp has been sent to your registered Mobile No';
				// _self.openModal(success.success.data.id);
			},(error)=>{
				_self.errSuccObj = {};
				_self.errSuccObj.error = error.message;
			}
		);
	}

	checkResetOtpWebUser(resForm){
		if(resForm.valid){
			let btn = $("#resOtpBtn");
	     	btn.button('loading');
	     	this.errSuccObj = {};
			this.peopleApi.checkResetOtpWebUser(this.peopleId,parseInt(this.otp),this.firebaseToken).subscribe((success)=>{
				btn.button('reset');
				this.openCPModal.emit({peopleId:this.peopleId, otp:this.otp});				
			},(error)=>{
				btn.button('reset');
				this.errSuccObj = {};
				if(this.localeId == "hi"){
					if(this.errMessages[error.message]){
						this.errSuccObj.error  = this.errMessages[error.message];
					}else{
						this.errSuccObj.error  = error.message;	
					}	
				}
				else{
					this.errSuccObj.error  = error.message;	
				}
				// this.errSuccObj.error = error.message;
			})	
		}
	}

	openModal(peopleId){
		this.errSuccObj = {};
		this.resOtpForm.resetForm();
		this.peopleId = peopleId;
		$("#reset-otp-modal").modal("show");
	}
	
	closeModal(){
		$("#reset-otp-modal").modal("hide");	
	}


}
