import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
// import { CategoryApi } from './../../sdk/index';

@Component({
  selector: 'app-server-error',
  templateUrl: './server-error.component.html',
  styleUrls: ['./server-error.component.css']
})
export class ServerErrorComponent implements OnInit {

  constructor(private _location : Location) { }

	ngOnInit() {
		//this.getAllCategories();
	}
  
	checkForUrl(){
		this._location.back();
	}
	
	/*getAllCategories(){
		let _self = this;
		_self.categoryApi.allData().subscribe(
			(success)=>{
				console.log("all categories : ", success);
				_self.checkForUrl();
			},
			(error)=>{
				//console.log("error : ", error);
				if(error === 'Server error'){
					
				}else{
					_self.checkForUrl();
				}
			}
		);
	}*/

}
