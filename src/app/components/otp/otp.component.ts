import { Component, OnInit, Output, EventEmitter , LOCALE_ID, Inject} from '@angular/core';

import { ReactiveFormsModule,FormsModule,FormGroup,FormControl,FormArray,Validators,FormBuilder } from '@angular/forms';
import { GlobalFunctionService } from './../../services/global-function.service';
import { PeopleApi, LoopBackAuth } from './../../sdk/index';

declare var $:any;

@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.css']
})
export class OtpComponent implements OnInit {
  
	otpForm  : FormGroup;
	otp 	 : FormControl; 
	peopleId : String;
	mode 	 : any;
	disableSubmit : boolean = false;
	//@Output() newData = new EventEmitter();
	//@Output() newData1 = new EventEmitter();
	
	errSuccObj : any = {};
	errMessages :any = {
		"Otp doesn't match":"ओटीपी मेल नहीं खाता है",
		"Value is not a number.": "कृपया नंबर दर्ज करे"
	}
	constructor(@Inject(LOCALE_ID) protected localeId: string, private peopleApi : PeopleApi,private globalFunctionService : GlobalFunctionService) { }

	ngOnInit(){
		this.createOtpForm();
	}
	
	openModal(peopleId){
		//this.disableSubmit = false
		if($('#otp-modal')[0].style.display === 'none' || $('#otp-modal')[0].style.display === ''){
			$('#otp-modal').modal({backdrop: 'static', keyboard: false}); 
			$('.modal-backdrop').show();
			// this.peopleId = this.peopleId || peopleId;
			this.peopleId = peopleId;
		}
	}
	
	openModal1(data){
		this.mode = data.mode;
		if($('#otp-modal')[0].style.display === 'none' || $('#otp-modal')[0].style.display === ''){
			$('#otp-modal').modal({backdrop: 'static', keyboard: false}); 
			$('.modal-backdrop').show();
		}
	}
	
	createOtpForm(){
		this.otpForm = new FormGroup({
			otp : new FormControl('',[Validators.required])
		});
	}
	
	resendOtp(peopleId,type){
		let _self = this;
		// this.peopleId = this.peopleId || peopleId;
		this.peopleId = peopleId;
		this.peopleApi.resendOtp(this.peopleId,type).subscribe(
			(success)=>{
				console.log("success : ", success);

				_self.errSuccObj.success =this.localeId == "hi" ? "नया ओटीपी आपके पंजीकृत मोबाइल नंबर पर भेजा गया है" : 'New Otp has been sent to your registered Mobile No';
				_self.openModal(success.success.data.id);
			},
			(error)=>{
				console.log("error : ", error);
				if(error === 'Server error'){
					_self.globalFunctionService.navigateToError('server');
				}else if(error.statusCode === 401){
					_self.globalFunctionService.navigateToError('401');
				}else{
					_self.errSuccObj = {};
					_self.errSuccObj.error = error.message;
				}
			}
		);
	}
	
	closeModal(){
		this.mode = '';
		this.errSuccObj = {};
		$('#otp-modal').modal('hide');
		this.otpForm.reset();
	}
	
	verifyOtp(){
		let _self = this;
		this.disableSubmit = true;
		if(this.otpForm.valid){
			this.peopleApi.verifyMobile(this.peopleId,+this.otpForm.value.otp,'').subscribe(
				(success)=>{
					console.log("mobile updated : ", success);

					_self.errSuccObj.success = this.localeId == "hi"? "मोबाइल सफलतापूर्वक सत्यापित किया गया है" : 'Mobile is successfully verified';
					setTimeout(()=>{
						_self.disableSubmit = false;
						_self.closeModal();
					},1000);
				},
				(error)=>{
					console.log("error : ", error);
					_self.disableSubmit = false;
					if(error === 'Server error'){
						_self.globalFunctionService.navigateToError('server');
					}else if(error.statusCode === 401){
						_self.globalFunctionService.navigateToError('401');
					}else{
						_self.errSuccObj = {};
						if(this.localeId == "hi"){
							if(this.errMessages[error.message]){
								 _self.errSuccObj.error = this.errMessages[error.message];
							}else{
								 _self.errSuccObj.error = error.message;	
							}	
						}
						else{
							this.errSuccObj.error  = error.message;	
						}
						// _self.errSuccObj.error = error.message;
					}
				}
			);
		}
	}

}
