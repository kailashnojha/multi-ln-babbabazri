import { Component, OnInit, LOCALE_ID, Inject  } from '@angular/core';
import { ReactiveFormsModule,FormsModule,FormGroup,FormControl,FormArray,Validators,FormBuilder } from '@angular/forms';
import { MultipartService } from './../../services/multipart/multipart.service';
import { GlobalFunctionService } from './../../services/global-function.service';
import { validateEmail } from './../../validators/email.validator';
import { validateMobile } from './../../validators/mobile.validator';
import { validatePercentage } from './../../validators/percentage.validator';
import { validateNoOfEngineers } from './../../validators/numberOfEng.validator';
import { validateAccountNo } from './../../validators/accountNo.validator';
import { stateNameList, stateNameListHindi } from './../../state-list';
import { Router } from '@angular/router';

@Component({
  selector: 'app-employment',
  templateUrl: './employment.component.html',
  styleUrls: ['./employment.component.css']
})
export class EmploymentComponent implements OnInit {
	
	avaiPositions : any = [];
	avaiPositionsHiEn:any={};
	workEx : any = [];
	workExHiEn:any = {};
	qualifications : any = [];
	qualificationsHiEn : any = {};
	
	employmentForm : FormGroup;
	email : FormControl;
	post  : FormControl;
	name  : FormControl;
	DOB   : FormControl;
	contactNo : FormControl;
	gender : FormControl;
	fatherName : FormControl;
	perAddress : FormGroup;
	value : FormControl;
	district : FormControl;
	city : FormControl;
	tehsil : FormControl;
	state : FormControl;
	jobAtAddress : FormControl;
	education : FormControl;
	secPerc : FormControl;
	senSecPerc : FormControl;
	postGradPerc : FormControl;
	workExp : FormControl;
	gradPerc : FormControl;
	preferredCity : FormControl;
	preferredState : FormControl;
	preferredJobAdd : FormGroup;
	
	files : any = {};
	stateList : any = [];
	stateListHindi : any = [];
	genderHindi:any ={};
	noSign : boolean = false;
	noProfilePic : boolean = false;
	
	errSuccObj : any = {};
	myInfo : any = {};
	isForm : boolean = false;
	hide : boolean = true;
	disabled : boolean = false;
	constructor(@Inject(LOCALE_ID) protected localeId: string, private router : Router,private globalFunctionService : GlobalFunctionService, private multipartService : MultipartService){
		this.stateList = stateNameList;
		this.stateListHindi = stateNameListHindi;
		this.avaiPositions = [
			"MARKETING MANAGER",
			"FIELD INSPECTOR", 
			"SOFTWARE ENGINEER", 
			"PURCHASE MANAGER",
			"LEGAL ADVISER", 
			"MEDIA INCHARGE", 
			"TRANSPORT INSPECTOR", 
			"MECHANICAL MANAGER", 
			"FINANCE MANAGER", 
			"H R MANAGER", 
			"SECURITY MANAGER" ,
			"DATA ENTRY OPERATOR CUM TYPIST" ,
			"INSURANCE AGENT", 
			"RECEPTIONIST" ,
			"COSTUMER CARE EXECUTIVE", 
			"MOTOR VEHICLE MECHANIC", 
			"SECURITY GUARD", 
			"PUNCTURE MAKER", 
			"HELPER", 
			"OFFICE BOY", 
			"PEON", 
			"DRIVER"
		];

		this.avaiPositionsHiEn = {
			"MARKETING MANAGER"                : "मार्केटिंग मैनेजर",
			"FIELD INSPECTOR"                  : "फील्ड इंस्पेक्टर", 
			"SOFTWARE ENGINEER"                : "सॉफ्टवेयर इंजीनियर", 
			"PURCHASE MANAGER"                 : "परचेस मैनेजर",
			"LEGAL ADVISER"                    : "लीगल एडवीज़र", 
			"MEDIA INCHARGE"                   : "मीडिया इंचार्ज", 
			"TRANSPORT INSPECTOR"              : "ट्रांसपोर्ट इंस्पेक्टर", 
			"MECHANICAL MANAGER"               : "मैकेनिकल मैनेजर", 
			"FINANCE MANAGER"                  : "फाइनेंस मैनेजर", 
			"H R MANAGER"                      : "एच आर मैनेजर", 
			"SECURITY MANAGER"                 : "सिक्योरिटी मैनेजर",
			"DATA ENTRY OPERATOR CUM TYPIST"   : "डाटा एंट्री ऑपरेटर कम टाइपिस्ट ",
			"INSURANCE AGENT"                  : "इन्शुरन्स एजेंट", 
			"RECEPTIONIST"                     : "रिसेप्शनिस्ट",
			"COSTUMER CARE EXECUTIVE"          : "कस्टमर केयर एग्जीक्यूटिव", 
			"MOTOR VEHICLE MECHANIC"           : "मोटर व्हीकल मैकेनिक", 
			"SECURITY GUARD"                   : "सिक्योरिटी गार्ड", 
			"PUNCTURE MAKER"                   : "पंक्चर मेकर", 
			"HELPER"                           : "हेल्पर", 
			"OFFICE BOY"                       : "ऑफिस बॉय", 
			"PEON"                             : "पेओन", 
			"DRIVER"                           : "ड्राइवर" 
		}

		
		this.workEx = [
			'0-1 Year', 
			'1-2 Year', 
			'2-3 Year', 
			'3-5 Year', 
			'5-10 Year', 
			'10+ Year'
		];

		this.workExHiEn = {
			'0-1 Year'  : "0-1 साल", 
			'1-2 Year'  : "1-2 साल", 
			'2-3 Year'  : "2-3 साल", 
			'3-5 Year'  : "3-5 साल", 
			'5-10 Year' : "5-10 साल", 
			'10+ Year'  : "10+ साल"
		}
		
		this.qualifications = [
			'NIL',
			'8TH PASS',
			'10TH PASS',
			'12TH PASS',
			'GRADUATION',
			'POST GRADUATION'
		];

		this.qualificationsHiEn = {
			'NIL'             : "शून्य",
			'8TH PASS'        : "8 वीं पास",
			'10TH PASS'       : "10 वीं पास",
			'12TH PASS'       : "12 वीं पास",
			'GRADUATION'      : "स्नातक स्तर",
			'POST GRADUATION' : "स्नातकोत्तर"
		}

		this.genderHindi = {
			male     : "पुरुष",
			female   : "महिला"
		}
	}

	onSubmit(){
		let _self = this;
		this.errSuccObj = {};
		console.log("form data : ", this.employmentForm.value);
		if(this.employmentForm.valid && this.files.profilePic && this.files.signImg){
			this.disabled = true;
			/*console.log("form data : ", this.employmentForm.value);
			console.log("file : ", this.files);*/
			let data = { 'data' : JSON.parse(JSON.stringify(this.employmentForm.value)), 'files' : this.files };

			this.setHiValue(data.data);
			// return;
			this.multipartService.employmentApi(data).subscribe(
				(success)=>{
					console.log("data added : ", success);
					_self.errSuccObj.success = this.localeId == "hi" ? "डेटा सफलतापूर्वक जोड़ा गया" : "Data Added Successfully";
					_self.hide = false;
					_self.disabled = false;
					_self.resetForm();
					_self.router.navigate(['dashboard']);
				},
				(error)=>{
					_self.disabled = false;
					if(error === 'Server error'){
						_self.globalFunctionService.navigateToError('server');
					}else if(error.statusCode === 401){
						_self.globalFunctionService.navigateToError('401');
					}else{
						_self.errSuccObj.error = error.message;
					}
				}
			);
		}else{
			this.employmentForm.controls['email'].markAsTouched();
			this.employmentForm.controls['post'].markAsTouched();
			this.employmentForm.controls['name'].markAsTouched();
			this.employmentForm.controls['DOB'].markAsTouched();
			this.employmentForm.controls['contactNo'].markAsTouched();
			this.employmentForm.controls['fatherName'].markAsTouched();
			this.perAddress.controls['value'].markAsTouched();
			this.perAddress.controls['district'].markAsTouched();
			this.perAddress.controls['city'].markAsTouched();
			this.perAddress.controls['tehsil'].markAsTouched();
			if(this.employmentForm.value.preferredJobAdd)
				this.preferredJobAdd.controls['preferredCity'].markAsTouched();
			
			if(this.employmentForm.value.education === '10TH PASS'){
				this.employmentForm.controls['secPerc'].markAsTouched();
			}
			else if(this.employmentForm.value.education === '12TH PASS'){
				this.employmentForm.controls['secPerc'].markAsTouched();
				this.employmentForm.controls['senSecPerc'].markAsTouched();
			}
			else if(this.employmentForm.value.education === 'GRADUATION'){
				this.employmentForm.controls['secPerc'].markAsTouched();
				this.employmentForm.controls['senSecPerc'].markAsTouched();
				this.employmentForm.controls['gradPerc'].markAsTouched();
			}
			else if(this.employmentForm.value.education === 'POST GRADUATION'){
				this.employmentForm.controls['secPerc'].markAsTouched();
				this.employmentForm.controls['senSecPerc'].markAsTouched();
				this.employmentForm.controls['gradPerc'].markAsTouched();
				this.employmentForm.controls['postGradPerc'].markAsTouched();
			}
				
			if(!this.files.profilePic)
				this.noProfilePic = true;
			if(!this.files.signImg)
				this.noSign = true;
		}
	}
	

	setHiValue(data){

		
		if(this.localeId != "hi")
			return;

		if(data.post)
			data.post = this.avaiPositionsHiEn[data.post];
		
		if(data.gender)
			data.gender = this.genderHindi[data.gender];
		
		if(data.perAddress && data.perAddress.state)
			data.perAddress.state = this.stateListHindi[data.perAddress.state];
		
		if(data.jobAtAddress)
			data.jobAtAddress = data.jobAtAddress == "yes" ? "हाँ" : "नहीं";
		
		if(data.preferredJobAdd && data.preferredJobAdd.preferredState)
			data.preferredJobAdd.preferredState = this.stateListHindi[data.preferredJobAdd.preferredState];

		if(data.education)
			data.education = this.qualificationsHiEn[data.education];

		if(data.workExp)
			data.workExp= this.workExHiEn[data.workExp];
	}


	ngOnInit(){
		this.errSuccObj = {};
		this.getMyInfo();
	}
	
	getMyInfo(){
		let _self = this;
		this.isForm = true;
		this.multipartService.getMyInfo().subscribe(
			(success)=>{
				console.log("my info : ", success);
				_self.myInfo = success.success.data;
				if(_self.myInfo.realm === 'webuser'){
					if(!_self.myInfo.employmentDone){
						_self.createForm();
					}else{
						_self.isForm = false;
					}
				}else{
					_self.createForm();
				}
			},
			(error)=>{
				console.log("error : ", error);
			}
		);
	}
	
	resetForm(){
		this.employmentForm.reset();
		this.perAddress.reset();
		this.employmentForm.controls['jobAtAddress'].setValue('yes');
		this.employmentForm.controls['education'].setValue('NIL');
		this.employmentForm.controls['gender'].setValue('male');
		this.employmentForm.controls['workExp'].setValue('0-1 Year');
		this.perAddress.controls['state'].setValue(this.stateList[0]);
		this.employmentForm.controls['post'].setValue(this.avaiPositions[0]);
		this.files = {};
		setTimeout(()=>{
			if(!this.hide){
				this.hide = true;
			}
		},10000);
	}
	
	createForm(){
		this.email = new FormControl('',[
			Validators.required,
			validateEmail
		]);
		this.post = new FormControl(this.avaiPositions[0],[
			Validators.required
		]);
		this.name = new FormControl('',[
			Validators.required
		]);
		this.DOB = new FormControl('',[
			Validators.required
		]);
		this.contactNo = new FormControl('',[
			Validators.required,
			validateMobile
		]);
		this.fatherName = new FormControl('',[
			Validators.required
		]);
		this.value = new FormControl('',[
			Validators.required
		]);
		this.district = new FormControl('',[
			Validators.required
		]);
		this.city = new FormControl('',[
			Validators.required
		]);
		this.tehsil = new FormControl('',[
			Validators.required
		]);
		this.state = new FormControl(this.stateList[0],[
			Validators.required
		]);
		this.perAddress = new FormGroup({
			value : this.value,
			district : this.district,
			city : this.city,
			tehsil : this.tehsil,
			state : this.state
		});
		this.preferredCity = new FormControl('',[
			Validators.required
		]);
		this.preferredState = new FormControl(this.stateList[0],[
			Validators.required
		]);
		this.jobAtAddress = new FormControl('yes',[
			Validators.required
		]);
		this.education = new FormControl('NIL',[
			Validators.required
		]);
		this.gender = new FormControl('male',[
			Validators.required
		]);
		this.workExp = new FormControl('0-1 Year',[
			Validators.required
		]);
		this.secPerc = new FormControl('',[
			validatePercentage,
			Validators.required
		]);
		this.senSecPerc = new FormControl('',[
			validatePercentage,
			Validators.required
		]);
		this.gradPerc = new FormControl('',[
			validatePercentage,
			Validators.required
		]);
		this.postGradPerc = new FormControl('',[
			validatePercentage,
			Validators.required
		]);
		
		this.preferredJobAdd = new FormGroup({
			preferredCity 	: this.preferredCity,
			preferredState 	: this.preferredState
		});
		
		this.employmentForm = new FormGroup({
			email: this.email,
			post : this.post,
			name : this.name,
			DOB  : this.DOB,
			contactNo : this.contactNo,
			gender : this.gender,
			fatherName : this.fatherName,
			perAddress : this.perAddress,
			jobAtAddress : this.jobAtAddress,
			education : this.education,
			workExp : this.workExp
		});
	}
	
	toggleAddress(){
		if(this.employmentForm.value.jobAtAddress === 'no'){
			this.employmentForm.addControl('preferredJobAdd', this.preferredJobAdd);
		}else{
			this.employmentForm.removeControl('preferredJobAdd');
		}
	}
	
	toggleEdu(){
		if(this.employmentForm.value.education === 'NIL' || this.employmentForm.value.education === '8TH PASS'){
			this.employmentForm.removeControl('secPerc');
			this.employmentForm.removeControl('senSecPerc');
			this.employmentForm.removeControl('gradPerc');
			this.employmentForm.removeControl('postGradPerc');
		}
		else if(this.employmentForm.value.education === '10TH PASS'){
			this.employmentForm.addControl('secPerc',this.secPerc);
			this.employmentForm.removeControl('senSecPerc');
			this.employmentForm.removeControl('gradPerc');
			this.employmentForm.removeControl('postGradPerc');
		}
		else if(this.employmentForm.value.education === '12TH PASS'){
			this.employmentForm.addControl('secPerc',this.secPerc);
			this.employmentForm.addControl('senSecPerc',this.senSecPerc);
			this.employmentForm.removeControl('gradPerc');
			this.employmentForm.removeControl('postGradPerc');
		}
		else if(this.employmentForm.value.education === 'GRADUATION'){
			this.employmentForm.addControl('secPerc',this.secPerc);
			this.employmentForm.addControl('senSecPerc',this.senSecPerc);
			this.employmentForm.addControl('gradPerc',this.gradPerc);
			this.employmentForm.removeControl('postGradPerc');
		}
		else if(this.employmentForm.value.education === 'POST GRADUATION'){
			this.employmentForm.addControl('secPerc',this.secPerc);
			this.employmentForm.addControl('senSecPerc',this.senSecPerc);
			this.employmentForm.addControl('gradPerc',this.gradPerc);
			this.employmentForm.addControl('postGradPerc',this.postGradPerc);
		}
	}
}
