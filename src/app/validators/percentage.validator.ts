import { AbstractControl } from '@angular/forms';

export function validatePercentage(control: AbstractControl){
	//var re = /^[0-9]\d{0,2}(\.\d{1,2})?%?$/;			
	var re = /(^100(\.0{1,2})?$)|(^([1-9]([0-9])?|0)(\.[0-9]{1,2})?$)/i;
	if (control.value && !re.test(control.value)){
		return { validPer: true };
	}
	return null;
}