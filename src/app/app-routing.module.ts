import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StartComponent } from './components/start/start.component';
import { ServerErrorComponent } from './components/server-error/server-error.component';
import { ForgetPasswordComponent } from './components/forget-password/forget-password.component';
import { NewPasswordComponent } from './components/new-password/new-password.component';

import { EmploymentComponent } from './components/employment/employment.component';
import { DealershipComponent } from './components/dealership/dealership.component';
import { VehicleComponent } from './components/vehicle/vehicle.component';
import { ContactComponent } from './components/contact/contact.component';
import { AboutUsComponent } from './components/about-us/about-us.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { PrivacyPolicyComponent } from './components/privacy-policy/privacy-policy.component';

const routes: Routes = [
	{ path: '', component: StartComponent, pathMatch: 'full'},
	{ path: 'server-error', component : ServerErrorComponent },
	{ path: 'employment', component : EmploymentComponent },
	{ path: 'dealership', component : DealershipComponent },
	{ path: 'vehicle', component : VehicleComponent },
	{ path: 'contact-us', component : ContactComponent },
	{ path: 'about-us', component : AboutUsComponent },
	{ path: 'dashboard', component : DashboardComponent },
	{ path: 'privacy-policy', component : PrivacyPolicyComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports : [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
